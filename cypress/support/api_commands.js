/// <reference types="Cypress" />

const accessToken = Cypress.env('gitlab_access_token');

Cypress.Commands.add('apiCreateProject', project => {
  cy.request({
    method: 'POST',
    url: `api/v4/projects/?private_token=${accessToken}`,
    body: {
      name: project.name,
      description: project.description,
      initialize_with_readme: true,
    }
  })
})

Cypress.Commands.add('apiCreateIssue', issue => {
  cy.apiCreateProject(issue.project)
    .then(response => {
      cy.request({
        method: 'POST',
        url: `api/v4/projects/${response.body.id}/issues?private_token=${accessToken}`,
        body: {
          title: issue.title,
          description: issue.description,
        }
      })
    });
})

Cypress.Commands.add('apiCreateLabel', (projectId, label) => {
  cy.request({
    method: 'POST',
    url: `/api/v4/projects/${projectId}/labels?private_token=${accessToken}`,
    body: {
      name: label.name,
      color: label.color
    }
  })
})

Cypress.Commands.add('apiCreateMilestone', (projectId, milestone) => {
  cy.request({
    method: 'POST',
    url: `/api/v4/projects/${projectId}/milestones?private_token=${accessToken}`,
    body: { title: milestone.title }
  })
})

