/// <reference types="Cypress" />

const faker = require('faker');

describe('Set Label On Issue', () => {

  const issue = {
    title: `issue-${faker.datatype.uuid()}`,
    description: faker.random.words(3),
    project: {
      name: `project-${faker.datatype.uuid()}`,
      description: faker.random.words(5)
    }
  }

  const label = {
    name: `label-${faker.random.words(1)}`,
    color: `#ffaabb`
  }

  beforeEach(() => {
    cy.login();
    cy.apiCreateIssue(issue)
      .then(response => {
        cy.apiCreateLabel(response.body.project_id, label);
        cy.visit(`${Cypress.env('user_name')}/${issue.project.name}/issues/${response.body.iid}`);
      });

  })

  it('successfully', () => {
    cy.guiSetLabelOnIssue(label);
    cy.get('.qa-labels-block').should('contain', label.name);
    cy.get('.qa-labels-block a span').should('have.attr', 'style', `background-color: ${label.color}; color: #333333`)
  })
});