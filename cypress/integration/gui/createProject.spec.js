/// <reference types="Cypress" />

const faker = require('faker');

describe('Create project', () => {
  beforeEach(() => {
    cy.login();
  });

  it('successfully', () => {
    const project = {
      name: `project-${faker.random.uuid()}`,
      description: faker.random.words(5),
    }

    cy.guiCreateProject(project);

    cy.url().should('be.equal', `${Cypress.config('baseUrl')}${Cypress.env('user_name')}/${project.name}`);
    cy.contains(project.name).should('be.visible');
    cy.contains(project.description).should('be.visible');
    cy.contains(`Project '${project.name}' was successfully created.`).should('be.visible');
  });
});